<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
	protected $fillable = [ 
							'fullname', 
							'dob', 
							'gender', 
							'salary', 
							'designation', 
							'image_path' , 
							'created_by', 
							'updated_by' 
						];
}
