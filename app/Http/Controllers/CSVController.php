<?php

namespace App\Http\Controllers;
use App\Employee;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CSVController extends Controller
{
    public function importCSV(Request $request)
    {
    	$skipped = "";
    	// dd($request->all());
    	foreach ($request->all()['parse_csv'] as $key => $value) {
    		if ($value['fullname'] == null && 
    			$value['dob'] == null && 
    			$value['gender'] == null && 
    			$value['salary'] == null &&
    			$value['designation'] == null
    		) {
    			if ($skipped == "") {
	    			$skipped = ($key+1).',';
    			}else{
	    			$skipped .= ($key+1).',';
    			}
    			continue;
    		}

    		$validator = Validator::make($value, [
    			"fullname" => 'required',
	            "dob" => 'required|date',
	            "gender" => 'required',
	            "salary" => 'required|min:0|numeric',
	            "designation" => 'required'
    		]);

	        if ($validator->fails()) {
	            return response()->json([	"key" => $key+1, 
	            							"messages" => $validator->errors()->all(), 
	            							'skipped' => mb_substr($skipped, 0, -1)]
	            							,500);
	        }

	    	$employee = Employee::create([
	    		'fullname' => $value['fullname'], 
				'dob' => date($value['dob']), 
				'gender' => $value['gender'], 
				'salary' => $value['salary'], 
				'designation' => $value['designation'], 
				'created_by' => Auth::user()->id, 
				'updated_by' => Auth::user()->id
	    	]);
    	}

    	return response()->json([ 	'key' => "",
    								'messages' => ['Successfully Imported'], 
    								'skipped' => mb_substr($skipped, 0, -1)]
    								,200);
    	
    }
}
