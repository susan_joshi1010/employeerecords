<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Datatables;
use DB;

class EmployeeController extends Controller
{

    public function updateEmployee(Request $request, $id)
    {
        // dd(strpos($request->avatar, 'employees') === false );
        $rules = [
            "fullname" => 'required',
            "dob" => 'required|date',
            "gender" => 'required',
            "salary" => 'required|min:0|numeric',
            "designation" => 'required'
        ];
 
        //check if the image is changed
        if (strpos($request->avatar, 'employees') === false) {
            $rules['avatar'] = 'img';
        }

        Validator::extend('img',function($attribute, $value, $params, $validator) {
            return $this->validateImage(explode(',',$value)[0]);
        });

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(["messages" => $validator->errors()->all()],500);
        }

        $data = [
            "fullname" => $request->fullname,
            "dob" => $request->dob,
            "gender" => $request->gender,
            "salary" => $request->salary,
            "designation" => $request->designation,
            "created_by" => Auth::user()->id,
            "updated_by" => Auth::user()->id,
        ];

        $image_path="";
        if (strpos($request->avatar, 'employees') === false) {
            $exploded = explode(',', $request->avatar);
            $decoded = base64_decode($exploded[1]);

            $fileName = time() . '.' . explode('/', explode(':', substr($request->avatar, 0, strpos($request->avatar, ';')))[1])[1];
            
            $image_path = public_path().'/employees/'.$fileName;
            file_put_contents($image_path, $decoded);
            $data["image_path"] = '/employees/'.$fileName;
        }

        $employee = Employee::where('id', $id)->update($data);
        return response()->json(["messages"=>["Employee Sucessfully Updated"]],200);
    }

    public function fetchEmployee($id)
    {
        $employee = Employee::where('id',$id)->first();
        return response()->json($employee);
    }

    public function fetchEmployees()
    {
        // $employee = Employee::select('id', 'fullname', 'dob', 'gender', 'designation', 'salary', 'created_at');
        $employee = DB::Select("Select id, fullname, dob, gender, designation, salary, date(created_at) as created_at from employees");
        return Datatables::of($employee)
         ->addColumn('checkbox', function ($employee)
         {
            
            $checkbox='<input type="checkbox" class="select" value="Bike"><br>';
            return $checkbox;
         })->addColumn('actions', function ($employee)
         {
            
            $buttons='<button type="button" id="'.$employee->id.'" class="update btn btn-primary" title="Update">&nbsp&nbsp Update</button>';
            return $buttons;
         })
         ->rawColumns(['checkbox', 'actions'])
         ->make(true);
    }

    public function validateImage($img_type)
    {
        // dd($img_type);
        switch ($img_type) {
            case 'data:image/png;base64':
                return true;
                break;
            case 'data:image/jpeg;base64':
                return true;
                break;
            case 'data:image/jpg;base64':
                return true;
                break;
            case 'data:image/gif;base64':
                return true;
                break;
            // case 'data:image/svg+xml;base64':
            //     break;
            // default:
                return false;
        }
    }

    public function addEmployee(Request $request)
    {
        // dd(gettype($request->dob),$request->dob);
        $rules = [
            "fullname" => 'required',
            "dob" => 'required|date',
            "gender" => 'required',
            "salary" => 'required|min:0|numeric',
            "designation" => 'required'
        ];
 
        if ($request->avatar!="http:/employees/profile-image.jpg") {
            $rules['avatar'] = 'img';
        }

        Validator::extend('img',function($attribute, $value, $params, $validator) {
            // dd($this->validateImage(explode(',',$value)[0]));
            return $this->validateImage(explode(',',$value)[0]);
        });

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(["messages" => $validator->errors()->all()],500);
        }
        $image_path="";
        $data = [
            "fullname" => $request->fullname,
            "dob" => $request->dob,
            "gender" => $request->gender,
            "salary" => $request->salary,
            "designation" => $request->designation,
            "created_by" => Auth::user()->id,
            "updated_by" => Auth::user()->id,
        ];

        if ($request->avatar!="http:/employees/profile-image.jpg") {
            $exploded = explode(',', $request->avatar);
            $decoded = base64_decode($exploded[1]);

            $fileName = time() . '.' . explode('/', explode(':', substr($request->avatar, 0, strpos($request->avatar, ';')))[1])[1];
            
            // if ($exploded[0] == 'data:image/svg+xml;base64') {
            // }
            // dd($exploded[0]);
            $image_path = public_path().'/employees/'.$fileName;
            file_put_contents($image_path, $decoded);
            $data["image_path"] = '/employees/'.$fileName;
        }

        $employee = Employee::create($data);
    	return response()->json(["messages"=>["Employee Sucessfully added"]],200);
    }
}
