<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class AuthController extends Controller
{
    public function fetchAuth()
    {
    	return response()->json(["auth"=>Auth::user()]);
    }
}
