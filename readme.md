How to start this project:
- 1) clone the project using: git clone git@bitbucket.org:susan_joshi1010/employeerecords.git
- 2) go to the project directory.
- 	a) run: composer install.
- 	b) make a copy of .env.example to .env. Configure database and run: php artisan migrate.
- 	c) run: php artisan key:generate
- 	d) run: npm install
-	e) run: npm install --save vue-router
- 	f) If you don't get any errors, run: php artisan serve followed by npm run watch-poll on your terminals in two terminal windows. The project should run at http://localhost:8000/


Feel Free to leave a message at susan.joshi1010@gmail.com if you experience any problems during running this project. Thank You. Happy coding.
