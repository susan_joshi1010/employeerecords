<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => '/employee-records','middleware'=>'auth'], function () {
      Route::get('/{vue_capture?}', function () {
              return view('master');
      })->where('vue_capture', '[\/\w\.-]*');
});

Auth::routes();

// Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => '/api','middleware'=>'auth'], function () {
	Route::get('/fetchAuth','AuthController@fetchAuth');
	Route::post('/addEmployee','EmployeeController@addEmployee');
	Route::post('/importCSV','CSVController@importCSV');
	Route::get('/fetchEmployees','EmployeeController@fetchEmployees');
	Route::get('/fetchEmployee/{id}','EmployeeController@fetchEmployee');
	Route::post('/updateEmployee/{id}','EmployeeController@updateEmployee');
});
