import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const router = new VueRouter({
      mode : 'history',
      auth: true,
      base: '/employee-records',
      routes: [
              { 
                path: '/login', 
                name: 'login' ,
                component: require('./components/login.vue'), 
                alias: 'login' 
              },{ 
                path: '/home', 
                name: 'home' ,
                component: require('./components/home.vue'), 
                alias: 'home' 
              },
              { 
                path: '/manage-employees/create/:id?', 
                name: 'manage-employees-create' ,
                component: require('./components/employees/create.vue'), 
              },
              { 
                path: '/manage-employees', 
                name: 'manage-employees-index' ,
                component: require('./components/employees/index.vue'), 
              },
          ]
})

export default router

