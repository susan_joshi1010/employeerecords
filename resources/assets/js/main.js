import Vue from 'vue';
import router from './router';
// import printThis from 'print-this';


// import VueRouter from 'vue-router';
// Vue.use(VueRouter);

// import VueAxios from 'vue-axios';
// import axios from 'axios';
const axios = require('axios');
import VueAxios from 'vue-axios';
window.jsPDF = require('jspdf');
 
Vue.use(VueAxios, axios)
// import lodash from 'lodash';
// import VueLodash from 'vue-lodash/dist/vue-lodash.min';
// import VueToastr from '@deveodk/vue-toastr';
// import '@deveodk/vue-toastr/dist/@deveodk/vue-toastr.css';
// import ToggleButton from 'vue-js-toggle-button';
// import VueSession from 'vue-session';
// Vue.use(VueSession);
// Vue.use(VueLodash, lodash);
// Vue.use(VueAxios, axios);
// Vue.use(require('vue-moment'));
// Vue.use(VueToastr);
// Vue.use(ToggleButton);

// Vue.prototype.trans = function(key){
//     return Vue._.get(window.trans, key, key);
// };
// Vue.prototype.config = function(key){
//     return Vue._.get(window.config, key, key);
// };

// Vue.filter('decimalFilter', function (val) {
// 	if (val >= 0) {
//             var val1 = val;
//             val = accounting.toFixed(val1,4);
//             return accounting.formatMoney(val, "", 2, ",", ".");
//       }else{
//             return accounting.formatMoney(0, "", 2, ",", ".");
//       }
// });

import App from './App.vue';
// import Example from './components/Example.vue';

// const router = new VueRouter({ mode: 'history', routes: routes});
new Vue(Vue.util.extend({ router }, App)).$mount('#app');
// new Vue({
//   el: '#root',
//   components: { App },
//   router,
//   render: h => h('app'),
// })